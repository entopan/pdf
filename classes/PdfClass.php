<?php namespace Entopancore\Pdf\Classes;

use Dompdf\Dompdf;
use Session;

class PdfClass
{

    use \October\Rain\Support\Traits\Singleton;


    /**
     * $view= vista da visualizzare nel pdf
     * $data= dati da inserire nella view
     * $modality= stream (il pdf verrà visualizzato), download (il pdf verrà scaricato)
     * $name= nome da dare al pdf scaricato
     * $resolution= dimensione foglio
     * $orientation= foglio
     **/

    public function generatePdfFromView($view, $data, $modality = "stream", $name = null, $resolution = "a4", $orientation = "portrait")
    {
        $makeview = \View::make($view, $data);
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($makeview)->setPaper($resolution, $orientation);
        switch ($modality) {
            case 'stream':
                return $pdf->stream();
                break;
            case 'download':
                return $pdf->download($name);
                break;
            case 'output':
                return $pdf->output();
                break;
            default:
                return $pdf->stream();
                break;
        }

    }

    public function generatePdf($pdfText, $modality = "stream", $name = null, $resolution = "a4", $orientation = "portrait")
    {
        $pdf = new Dompdf();
        switch ($modality) {
            case 'stream':
                return $pdf->stream($pdfText);
                break;
            case 'download':
                return $pdf->download($pdfText);
                break;
            case 'output':
                return $pdf->output($pdfText);
                break;
            default:
                return $pdf->stream();
                break;
        }
    }

    public function generatePdfFileFromView($view, $data, $name = "file")
    {
        $pdf = \PDF::loadView($view, $data);
        file_put_contents($name . ".pdf", $pdf->output());
    }

}