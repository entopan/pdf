<?php

namespace Entopancore\Pdf;

use System\Classes\PluginBase;


class Plugin extends PluginBase
{

    /**
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'entopancore.pdf::lang.plugin.name',
            'description' => 'entopancore.pdf::lang.plugin.description',
            'author' => 'Entopancore',
            'icon' => 'icon-file-pdf-o',
        ];
    }

    /**
     * @return void
     */
    public function boot()
    {
        \App::register(\Barryvdh\DomPDF\ServiceProvider::class);
        \Illuminate\Foundation\AliasLoader::getInstance()->alias('PDF', \Barryvdh\DomPDF\Facade::class);

    }

    /**
     * @return array
     */

    /**
     * @return array
     */
    public function registerPermissions()
    {
        return [

        ];
    }

}